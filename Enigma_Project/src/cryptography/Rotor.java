package cryptography;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 * 
 * @author Sarah MARECAR - M2 MIAGE FA
 * @className ROTOR
 *
 */
public class Rotor {

	private HashMap<Integer, Character> branchementRotor = new HashMap<Integer, Character>();
	private HashMap<Character, Character> associationCharacter = new HashMap<Character, Character>();
	private int n = 1;
	private int numeroRotor;
	private int positionInitiale;
	private int positionCourant;
	private int positionTrigger;
	private int positionMouvementCumulee;
	private int cumul = 1;
	private boolean choisi;

	
	/**
	 * @function Constructeur
	 * @param numeroRotor
	 * @param positionCourant
	 * @param positionTrigger
	 * @param positionMouvementCumulee
	 * @param choisi
	 */
	public Rotor(int numeroRotor, int positionCourant, int positionTrigger, int positionMouvementCumulee,
			boolean choisi) {
		this.numeroRotor = numeroRotor;
		this.positionCourant = positionCourant;
		this.choisi = choisi;
		this.positionTrigger = positionTrigger;
		branchementRotor = this.branchementRotor();
		//En guise de vérification des mouvements des rotors :
		this.positionMouvementCumulee = positionMouvementCumulee;

	}

	/**
	 * @function branchementRotor
	 * @description création de deux colonnes pour les Rotors : KEY : alphabet; VALUE : alphabet.shuffled (=choix arbitraire)
	 * @return branchementRotor - HashMap
	 */
	public HashMap<Integer, Character> branchementRotor() {

		Character[] carac = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
				's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
		//Si besoin de shuffler la configuration interne des rotors 
		//(=choix arbitraire car dans les rotors normalement il existe des plaquette d'un embout à un autre d'un rotor qui relie une lettre à une autre plaquette branché à d'autres lettres) (CF. VOIR NUMBERPHILE (Flaws of Enigma):
		Collections.shuffle(Arrays.asList(carac));

		if (!branchementRotor.isEmpty()) {
			branchementRotor.clear();
		} else {
			for (char alpha : carac) {
				branchementRotor.put(n++, alpha);
			}
		}
		return branchementRotor;
	}

	/**
	 * @function shiftPosition
	 * @description Mouvement des lettres dans un Rotor  EXEMPLE : Position 0 (a-b-c-d) => Position 1 (b-c-d-a)
	 * @param position
	 * @return branchementRotor - HashMap
	 */
	public HashMap<Integer, Character> shiftPosition(int position) {
		char tmp = 0;
		int n = 0;

		for (Entry<Integer, Character> rot : branchementRotor.entrySet()) {
			n++;
			switch (rot.getKey()) {
			case 1:
				tmp = rot.getValue();

				branchementRotor.put(1, branchementRotor.get(2));
				break;
			case 26:
				branchementRotor.put(26, tmp);
				break;
			default:
				branchementRotor.put(n, branchementRotor.get(n + 1));
				break;
			}
			setPositionCourant(position);

			if (getPositionCourant() > branchementRotor.size()) {
				setPositionCourant(1);
				setPositionCumulee(1);
			}

		}

		if (!(getPositionCumulee() > branchementRotor.size())) {
			// System.out.println("JE RENTRE DEDANS");
			setPositionCumulee(cumul++);
			if (cumul > branchementRotor.size()) {
				cumul = 1;
			}
		}
		// System.out.println("ATTENTION CUMUL PASSAGER POUR LE ROTOR :" +
		// this.getNumeroRotor() + " CUMUL => " + this.getPositionCumulee() );
		associateAlphabetRotor();
		return branchementRotor;

	}

	/**
	 * @function Association avec des lettres avec le Hashmap BranchementRotor EXEMPLE : branchementRotor {1=a, 2=b, 3=c, 4=d) => AssociationAlphabetRotor {a=a, b=b, c=c, d=d} 
	 * @return associationCharacter - HashMap
	 */
	public HashMap<Character, Character> associateAlphabetRotor() {

		char[] carac = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
				't', 'u', 'v', 'w', 'x', 'y', 'z' };

		for (int i = 1; i <= carac.length; i++) {
			for (Entry<Integer, Character> rot : branchementRotor.entrySet()) {
				if (i == rot.getKey()) {
					associationCharacter.put(carac[i - 1], rot.getValue());
				}
			}

		}
		return associationCharacter;
	}

	/**
	 * 
	 * @return branchementRotor
	 */
	public HashMap<Integer, Character> getBranchement() {

		return branchementRotor;
	}

	/**
	 * 
	 * @param rotor
	 */
	public void setBranchement(HashMap<Integer, Character> rotor) {

		this.branchementRotor = rotor;
	}

	/**
	 * 
	 * @return numeroRotor
	 */
	public int getNumeroRotor() {
		return numeroRotor;
	}

	/**
	 * 
	 * @param numeroRotor
	 */
	public void setNumeroRotor(int numeroRotor) {
		this.numeroRotor = numeroRotor;
	}

	/**
	 * 
	 * @return choisi
	 */
	public boolean isChoisi() {
		return choisi;
	}

	/**
	 * 
	 * @param choisi
	 */
	public void setChoisi(boolean choisi) {
		this.choisi = choisi;
	}

	/**
	 * 
	 * @return associationCharacter
	 */
	public HashMap<Character, Character> getAssociationCharacter() {
		return associationCharacter;
	}

	/**
	 * 
	 * @param associationCharacter
	 */
	public void setAssociationCharacter(HashMap<Character, Character> associationCharacter) {
		this.associationCharacter = associationCharacter;
	}

	/**
	 * 
	 * @return positionInitiale
	 */
	public int getPositionInitiale() {
		return positionInitiale;
	}

	/**
	 * 
	 * @param positionInitiale
	 */
	public void setPositionInitiale(int positionInitiale) {
		this.positionInitiale = positionInitiale;
	}

	/**
	 * 
	 * @return positionCourant
	 */
	public int getPositionCourant() {
		return positionCourant;
	}

	/** 
	 * 
	 * @param positionCourant
	 */
	public void setPositionCourant(int positionCourant) {
		this.positionCourant = positionCourant;
	}

	/**
	 * 
	 * @return positionTrigger
	 */
	public int getPositionTrigger() {
		return positionTrigger;
	}

	/**
	 * 
	 * @param positionTrigger
	 */
	public void setPositionTrigger(int positionTrigger) {
		this.positionTrigger = positionTrigger;
	}

	/**
	 * 
	 * @return positionMouvementCumulee
	 */
	public int getPositionCumulee() {
		return positionMouvementCumulee;
	}

	/**
	 * 
	 * @param positionCumulee
	 */
	public void setPositionCumulee(int positionCumulee) {
		this.positionMouvementCumulee = positionCumulee;
	}

}