package cryptography;

import java.util.HashMap;
import java.util.Map.Entry;

/**
 * 
 * @author Sarah MARECAR - M2 MIAGE FA
 * @className PlugBoard - Tableau de connexion
 *
 */
public class PlugBoard {

	private HashMap<Character, Character> plugboard = new HashMap<Character, Character>();
	private boolean estPasse = false;

	/**
	 * @function Constructeur
	 */
	public PlugBoard() {
		this.plugboard = plugBoardStandard();
	}

	/**
	 * @function PlugBoardStandard - Association input à deux colonnes alphabets K/V 
	 * @return plugboard - HashMap
	 */
	public HashMap<Character, Character> plugBoardStandard() {

		char[] alpha = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

		for (char priseLettre : alpha) {
			plugboard.put(priseLettre, priseLettre);
		}
		return plugboard;

	}

	/**
	 * @function Association alphabet - Branchement prise
	 * @param lettreX
	 * @param lettreY
	 * @return plugboard
	 */
	public HashMap<Character, Character> plugBoardBranche(char lettreX, char lettreY) {

		for (Entry<Character, Character> board : plugboard.entrySet()) {
			if (lettreX == board.getKey()) {
				plugboard.put(lettreX, lettreY);
			}
			if (lettreY == board.getKey()) {
				plugboard.put(lettreY, lettreX);
			}
		}
		return plugboard;

	}
	
	/**
	 * 
	 * @function getPlugAll
	 */

	public void getPlugAll() {
		for (Entry<Character, Character> branchementChoisis : getPlugboard().entrySet()) {
				System.out.println("La lettre " + branchementChoisis.getKey() + " est branchee avec la lettre "
						+ branchementChoisis.getValue());

		}
		
	}

	/**
	 * 
	 * @return plugboard
	 */
	public HashMap<Character, Character> getPlugboard() {
		return plugboard;
	}

	/**
	 * 
	 * @param plugboard
	 */
	public void setPlugboard(HashMap<Character, Character> plugboard) {
		this.plugboard = plugboard;
	}

	/**
	 * 
	 * @return estPasse
	 */
	public boolean isEstPasse() {
		return estPasse;
	}

	/**
	 * 
	 * @param estPasse
	 */
	public void setEstPasse(boolean estPasse) {
		this.estPasse = estPasse;
	}

}
