package cryptography;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


/**
 * 
 * @author Sarah MARECAR - M2 MIAGE FA
 * @className Reflecteur
 *
 */
public class Reflecteur {
	
	private HashMap<Character, Character> reflecteur = new HashMap<Character, Character>(); 
	private boolean estPasse;
	
	/**
	 * @function Constructeur
	 * @param estPasse
	 */
	public Reflecteur(boolean estPasse) {
		this.estPasse =  estPasse;
		this.reflecteur = createPairReflecteur();
	}
	
	/**
	 * @function création d'un réflecteur de lettres par paire (configuration par paire : choix arbitraire et constituant une des techniques des reflecteurs des machines Enigma)
	 * @return reflecteur - HashMap
	 */
	public HashMap<Character,Character> createPairReflecteur(){

		List<Character> pairedCarac = Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
		char[] sortCarac = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
		
		
		//Collections.shuffle((Arrays.asList(pairedCarac)));
		for (int i=0; i<pairedCarac.size();i+=2) {	
			
				Collections.swap(pairedCarac, i, i+1);
				
		}
			
			 
		for (int i=0; i<sortCarac.length;i++) {
			for (int j=0; j<pairedCarac.size(); j++) {
				reflecteur.put(sortCarac[i], pairedCarac.get(i));
			}
		}
		return reflecteur;
		
	}

	/**
	 * 
	 * @return reflecteur
	 */
	public HashMap<Character, Character> getReflecteur() {
		return reflecteur;
	}

	/**
	 * 
	 * @param reflecteur
	 */
	public void setReflecteur(HashMap<Character, Character> reflecteur) {
		this.reflecteur = reflecteur;
	}

	/**
	 * 
	 * @return estPasse
	 */
	public boolean isEstPasse() {
		return estPasse;
	}

	/**
	 * 
	 * @param estPasse
	 */
	public void setEstPasse(boolean estPasse) {
		this.estPasse = estPasse;
	}
	
	
}
