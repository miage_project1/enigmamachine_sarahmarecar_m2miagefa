package cryptography;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

/**
 * 
 * @author Sarah MARECAR - M2 MIAGE FA
 * @className Paquet Rotor
 * @calls Rotor
 * 
 */
public class PaquetRotor {

	private ArrayList<Rotor> allRotors;
	private HashMap<Integer, Rotor> rotorSelectionne = new HashMap<Integer, Rotor>();
	private HashMap<Integer, Rotor> rotorNonSelectionne = new HashMap<Integer, Rotor>();
	private HashMap<Integer, Rotor> rotorAll = new HashMap<Integer, Rotor>();
	private boolean hasChanged = false;
	private Random r = new Random();

	/**
	 * @function Constructeur
	 * @description Initialisation des 8 rotors
	 */
	public PaquetRotor() {

		allRotors = new ArrayList<>(Arrays.asList(
				new Rotor(1, 0, 0, 0, false), 
				new Rotor(2, 0, 0, 0, false),
				new Rotor(3, 0, 0, 0, false), 
				new Rotor(4, 0, 0, 0, false), 
				new Rotor(5, 0, 0, 0, false),
				new Rotor(6, 0, 0, 0, false), 
				new Rotor(7, 0, 0, 0, false),				
				new Rotor(8, 0, 0, 0, false)
				
				));

		this.rotorAll = getRotorAll();
	}

	/**
	 * @function Assigner par numeroRotor les positionDepart, positionTrigger aux Rotors choisis
	 * @param numeroRotor
	 * @param positionDepart
	 * @param positionTrigger
	 */
	public void rotorChoisi(int numeroRotor, int positionDepart, int positionTrigger) {
		int s = 1;
		for (Rotor rot : allRotors) {

			if (rot.getNumeroRotor() == numeroRotor) {
				rot.setChoisi(true);
				if (rot.isChoisi() == true) {
					if (rot.getPositionCourant() == 0 && rot.getPositionTrigger() == 0) {
						for (int i = 1; i <= positionDepart; i++) {
							System.out.println("\nLe Rotor a bouge " + i + " fois. \nVoici l'etat du Rotor:\n"
									+ rot.shiftPosition(positionDepart));
							System.out.println("Verification Mouvement Rotor  :" + rot.getPositionCumulee());
						}
					} else {

						do {
							System.out.println("\n\nNUMERO ROTOR MACHINE " + rot.getNumeroRotor() + "\n =>");
							System.out.println("\nLe Rotor a bouge " + s++ + " fois. \nVoici l'etat du Rotor:\n"
									+ rot.shiftPosition(positionDepart));
							System.out.println(
									"Verification Mouvement Rotor :" + rot.getPositionCumulee() + " POSITION DEPART " + positionDepart);

							if (rot.getNumeroRotor() == 3) {
								if (rot.getPositionCumulee() > rot.getPositionTrigger()) {
									setHasChanged(true);
								}
							}
						} while (rot.getPositionCumulee() != positionDepart);

						if (isHasChanged() == true && rot.isChoisi() == false) {
							do {
								System.out.println("\n\nNUMERO ROTOR MACHINE " + rot.getNumeroRotor() + "\n =>");
								System.out.println("\nLe Rotor a bouge " + s++ + " fois. \nVoici l'etat du Rotor:\n"
										+ rot.shiftPosition(positionDepart));
								System.out.println("Verification Mouvement Rotor :" + rot.getPositionCumulee() + " POSITION DEPART "
										+ positionDepart);

								if (rot.getNumeroRotor() == 3) {
									if (rot.getPositionCumulee() > rot.getPositionTrigger()) {
										setHasChanged(true);
									}
								}
							} while (rot.getPositionCumulee() != positionDepart);
						}

					}

					rot.setPositionInitiale(positionDepart);
					rot.setPositionTrigger(positionTrigger);

				}

			}
		}

	}
	
	/**
	 * @function Assigner les positions Trigger et position Depart aux 5 autres Rotors puis shifter
	 */

	public void setAutresRotors() {
		int positionInitiale = 0;
		int positionTriggered;
		for (Rotor rot : allRotors) {

			if (rot.isChoisi() == false) {
				if (rot.getPositionCourant() == 0 && rot.getPositionTrigger() == 0) {
					positionInitiale = r.nextInt(rot.getBranchement().size()) + 1;
					positionTriggered = r.nextInt(rot.getBranchement().size()) + 1;
					rot.setPositionInitiale(positionInitiale);
					rot.setPositionTrigger(positionTriggered);
					
					rot.setPositionInitiale(positionInitiale);
					rot.setPositionTrigger(positionTriggered);
					 System.out.println("\n\nNUMERO ROTOR MACHINE " + rot.getNumeroRotor());
					for (int i = 1; i <= rot.getPositionInitiale(); i++) {
						//rot.shiftPosition(rot.getPositionInitiale());
						 System.out.println("\nLe Rotor a bouge " + i + " fois. \nVoici l'etat du Rotor:\n"
						 + rot.shiftPosition(rot.getPositionInitiale())+ " Verification Mouvement Rotor : " + rot.getPositionCumulee());
					}
				} else {
					//System.out.println("RANIIIIIIIIIA");
					do {
						rot.shiftPosition(rot.getPositionInitiale());
//						System.out.println("\nNUMERO ROTOR MACHINE " + rot.getNumeroRotor()+"\n =>");
//						System.out.println("\nLe Rotor a bouge " + s++ + " fois. \nVoici l'etat du Rotor:\n"
//								+ rot.shiftPosition(rot.getPositionInitiale()));
//						System.out.println("COUCOU CUMUL :" + rot.getPositionCumulee() + " POSITION DEPART " + rot.getPositionInitiale());
//					
						if (rot.getNumeroRotor() == 3) {
							if (rot.getPositionCumulee() > rot.getPositionTrigger()) {
								setHasChanged(true);
							}
						}
					} while (rot.getPositionCumulee() != rot.getPositionInitiale());
				}
			}
		}
	}

/**
 * 
 * @return rotorAll - HashMap
 */
	public HashMap<Integer, Rotor> getAllRotors() {
		int n = 1;
		for (Rotor rot : allRotors) {
			rot.getBranchement();
			rotorAll.put(n++, rot);

		}
		return rotorAll;
	}

	/**
	 * UNUSED - AU CAS OU
	 * @return rotorSelectionne - HashMap
	 */
	public HashMap<Integer, Rotor> getAllRotorsChoisis() {

		int n = 1;

		for (Rotor rot : allRotors) {
			if (rot.isChoisi() == true) {

				rotorSelectionne.put(n++, rot);
			}

		}
		setRotorSelectionne(rotorSelectionne);

		return rotorSelectionne;
	}

	/**
	 * UNUSED - AU CAS OU
	 * @return rotorNonSelectionne - HashMap
	 */
	public HashMap<Integer, Rotor> getAllRotorsNonChoisis() {

		int n = 1;
		int positionCourant;
		int positionTrigger;
		for (Rotor rot : allRotors) {
			if (rot.isChoisi() == false) {
				rotorNonSelectionne.put(n++, rot);

			}
			positionCourant = r.nextInt(rot.getBranchement().size());
			positionTrigger = r.nextInt(rot.getBranchement().size());
			rot.setPositionCourant(positionCourant);
			rot.setPositionTrigger(positionTrigger);

		}
		setRotorAll(rotorNonSelectionne);

		return rotorNonSelectionne;
	}
	
	/**
	 * @function INVERTMAP 
	 * @description Pour le passage arrière du circuit : j'inverse mon hashmap
	 * @return RotorSelectionne - HashMap
	 */

	public HashMap<Integer, Rotor> invertMap() {
		HashMap<Integer, Rotor> rotorChoisisInverse = new HashMap<Integer, Rotor>();
		//NP/NPO =  Juste des compteurs 
		
		int np = 1; // car je veux que ma hashmap commence par 1
		int npo = 0; // pour getter ma première valeur de ma liste
		List<Map.Entry<Integer, Rotor>> list = new ArrayList<>(getRotorAll().entrySet());
		Collections.reverse(list);

		//Foreach uniquement pour retrouver les elements de ma hash, un peu une sorte de contournement
		for (@SuppressWarnings("unused") Entry<Integer, Rotor> newlist : list) {
			rotorChoisisInverse.put(np++, list.get(npo++).getValue());
		}

		this.setRotorAll(rotorChoisisInverse);
		return rotorSelectionne;

	}

	/**
	 * 
	 * @return rotorSelectionne
	 */
	public HashMap<Integer, Rotor> getRotorSelectionne() {

		return rotorSelectionne;
	}

	/**
	 * 
	 * @param rotorSelectionne
	 */
	public void setRotorSelectionne(HashMap<Integer, Rotor> rotorSelectionne) {
		this.rotorSelectionne = rotorSelectionne;
	}
	
	/**
	 * 
	 * @param allRotors
	 */

	public void setAllRotors(ArrayList<Rotor> allRotors) {
		this.allRotors = allRotors;
	}

	/**
	 * 
	 * @return hasChanged
	 */
	public boolean isHasChanged() {
		return hasChanged;
	}

	/**
	 * 
	 * @param hasChanged
	 */
	public void setHasChanged(boolean hasChanged) {
		for (Rotor rot : allRotors) {
			rot.setChoisi(false);
		}
		this.hasChanged = hasChanged;
	}

	/**
	 * 
	 * @return rotorNonSelectionne
	 */
	public HashMap<Integer, Rotor> getRotorNonSelectionne() {
		return rotorNonSelectionne;
	}

	/**
	 * 
	 * @param rotorNonSelectionne
	 */
	public void setRotorNonSelectionne(HashMap<Integer, Rotor> rotorNonSelectionne) {
		this.rotorNonSelectionne = rotorNonSelectionne;
	}

	/**
	 * 
	 * @return rotorAll
	 */
	public HashMap<Integer, Rotor> getRotorAll() {
		return rotorAll;
	}

	/**
	 * 
	 * @param rotorAll
	 */
	public void setRotorAll(HashMap<Integer, Rotor> rotorAll) {
		this.rotorAll = rotorAll;
	}

}
