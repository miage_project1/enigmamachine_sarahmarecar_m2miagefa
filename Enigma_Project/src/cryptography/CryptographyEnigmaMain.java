package cryptography;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;


import java.util.Scanner;
/**
 * 
 * @author Sarah MARECAR - M2 MIAGE FA 
 * @className CrytographyEnigmaMain
 * @calls PaquetRotor,PlugBoard, Reflecteur
 * {@summary = SHIFT ROTOR, CHIFFRER MESSAGE, MAIN }
 * POUR PLUS DE DETAILS, CONSULTEZ LA DOCUMENTATION -> DOSSIER DOC > INDEX.HTML
 */

public class CryptographyEnigmaMain {

	private static PaquetRotor rotors = new PaquetRotor();
	private static Reflecteur reflecteur = new Reflecteur(false);
	private static PlugBoard plugboard = new PlugBoard();
	private static List<Character> code = new ArrayList<Character>();


	private static boolean tourR1 = false;
	private static boolean tourR2 = false;
	private static boolean tourR3 = false;
	private static boolean tourR4 = false;
	private static boolean tourR5 = false;
	private static boolean tourR6 = false;
	private static boolean tourR7 = false;

	
	/**
	 * 
	 * @function shiftRotor
	 * @description Mouvement Rotor sur chaque input de lettre dans le circuit
	 * @param rotor
	 * @param lettreMsg
	 */
	public static void shiftRotor(Rotor rotor, char lettreMsg) {

		int positionTrigger;

		if (rotor.getNumeroRotor() == 1) {

			rotor.shiftPosition(rotor.getPositionCourant() + 1);

			positionTrigger = rotor.getPositionTrigger();
			// ICI ARRETE TOI LA
			System.out.println("\nMOUVEMENT ROTOR : " + rotor.getNumeroRotor() + " passe a la position "
					+ rotor.getPositionCourant() + " pour la lettre " + lettreMsg + ".");
			System.out.println("GROS TEST DE BINARY ROTOR 1: " + tourR1);
			if (rotor.getPositionCourant() == positionTrigger) {
				tourR1 = true;
				System.out.println("Le rotor est venu à sa position de trigger, le prochain rotor va bouger.");
			} else {
				tourR1 = false;
			}
		}

		if (rotor.getNumeroRotor() == 2) {
			if (tourR1 == true) {
				positionTrigger = rotor.getPositionTrigger();
				rotor.shiftPosition(rotor.getPositionCourant() + 1);
				System.out.println("\nMOUVEMENT ROTOR : " + rotor.getNumeroRotor() + " passe a la position "
						+ rotor.getPositionCourant() + " pour la lettre " + lettreMsg + ".");
				if (rotor.getPositionCourant() == positionTrigger) {
					tourR2 = true;
				}

			} else {
				tourR2 = false;
			}
		}
		if (rotor.getNumeroRotor() == 3) {
			if (tourR2 == true) {
				positionTrigger = rotor.getPositionTrigger();
				rotor.shiftPosition(rotor.getPositionCourant() + 1);
				System.out.println("\nMOUVEMENT ROTOR : " + rotor.getNumeroRotor() + " passe a la position "
						+ rotor.getPositionCourant() + " pour la lettre " + lettreMsg + ".");
				if (rotor.getPositionCourant() == positionTrigger) {
					tourR3 = true;
				}

			} else {
				tourR3 = false;
			}
		}
		
		if (rotor.getNumeroRotor() == 4) {
			if (tourR3 == true) {
				positionTrigger = rotor.getPositionTrigger();
				rotor.shiftPosition(rotor.getPositionCourant() + 1);
				System.out.println("\nMOUVEMENT ROTOR : " + rotor.getNumeroRotor() + " passe a la position "
						+ rotor.getPositionCourant() + " pour la lettre " + lettreMsg + ".");
				if (rotor.getPositionCourant() == positionTrigger) {
					tourR4 = true;
				}

			} else {
				tourR4 = false;
			}
		}
		
		if (rotor.getNumeroRotor() == 5) {
			if (tourR4 == true) {
				positionTrigger = rotor.getPositionTrigger();
				rotor.shiftPosition(rotor.getPositionCourant() + 1);
				System.out.println("\nMOUVEMENT ROTOR : " + rotor.getNumeroRotor() + " passe a la position "
						+ rotor.getPositionCourant() + " pour la lettre " + lettreMsg + ".");
				if (rotor.getPositionCourant() == positionTrigger) {
					tourR5 = true;
				}

			} else {
				tourR5 = false;
			}
		}
		
		if (rotor.getNumeroRotor() == 6) {
			if (tourR5 == true) {
				positionTrigger = rotor.getPositionTrigger();
				rotor.shiftPosition(rotor.getPositionCourant() + 1);
				System.out.println("\nMOUVEMENT ROTOR : " + rotor.getNumeroRotor() + " passe a la position "
						+ rotor.getPositionCourant() + " pour la lettre " + lettreMsg + ".");
				if (rotor.getPositionCourant() == positionTrigger) {
					tourR6 = true;
				}

			} else {
				tourR6 = false;
			}
		}
		
		if (rotor.getNumeroRotor() == 7) {
			if (tourR6 == true) {
				positionTrigger = rotor.getPositionTrigger();
				rotor.shiftPosition(rotor.getPositionCourant() + 1);
				System.out.println("\nMOUVEMENT ROTOR : " + rotor.getNumeroRotor() + " passe a la position "
						+ rotor.getPositionCourant() + " pour la lettre " + lettreMsg + ".");
				if (rotor.getPositionCourant() == positionTrigger) {
					tourR7 = true;
				}

			} else {
				tourR7 = false;
			}
		}
		
		if (rotor.getNumeroRotor() == 8) {
			if (tourR7 == true) {
				positionTrigger = rotor.getPositionTrigger();
				rotor.shiftPosition(rotor.getPositionCourant() + 1);
				System.out.println("\nMOUVEMENT ROTOR : " + rotor.getNumeroRotor() + " passe a la position "
						+ rotor.getPositionCourant() + " pour la lettre " + lettreMsg + ".");
//				if (rot.getPositionCourant() == positionTrigger) {
//					tourR8 = true;
//				}
//
//			} else {
//				tourR8 = false;
//			}
		}
	}



	}

/**
 * @function chiffrerMessage - Fonction de chiffrement/déchiffrement du message entré - chacune des lettres sont passés par le circuit
 * @description Passage par le Plugboard - 8 Rotors - Reflecteur - 8 Rotors - Plugboard
 * @param msgEnClair
 * @return code :une liste de character composée de lettres chiffrées 
 */
	public static List<Character> chiffrerMessage(String msgEnClair) {
		char tmpParcoursAller;
		char tmpParcoursRetour = 0;
		int compteur = 1;
		int compteurReflecteur = 1;

		for (int i = 0; i < msgEnClair.length(); i++) {

			//DO - WHILE : Recommencer tant que la lettre n'est pas passé par le réflecteur (soit 1 fois)
			do {
				tmpParcoursAller = msgEnClair.charAt(i);
				compteur += 1;
				//PASSAGE PLUGBOARD ALLER
				if (reflecteur.isEstPasse() == false) {
					System.out.println("\n=> Passe dans la Table de Connexion ALLER : " + plugboard.getPlugboard());
					for (Entry<Character, Character> table : plugboard.getPlugboard().entrySet()) {
						if (tmpParcoursAller == table.getKey()) {
							tmpParcoursAller = table.getValue();
							System.out.println("La lettre " + msgEnClair.charAt(i) + " devient " + tmpParcoursAller);
							plugboard.setEstPasse(true);
							break;
						}
					}
				}

				if (reflecteur.isEstPasse() == true) {
					System.out.println("\n On va faire chemin retour !!");
					rotors.invertMap();

				}
				//PASSAGE ROTORS - ALLER ET RETOUR
				for (Entry<Integer, Rotor> rot : rotors.getRotorAll().entrySet()) {

					System.out.println("Sommes-nous passes par le reflecteur ? => REPONSE  " + reflecteur.isEstPasse());
					if (reflecteur.isEstPasse() == false) {
						//APPEL DE LA FONCTION SHIFTROTOR
						shiftRotor(rot.getValue(), msgEnClair.charAt(i));
					}

					System.out.println("\n=> Passe dans le Rotor " + rot.getValue().getNumeroRotor());
					for (Entry<Character, Character> rotir : rot.getValue().getAssociationCharacter().entrySet()) {

						if (reflecteur.isEstPasse() == false) {
							if (tmpParcoursAller == rotir.getKey()) {
								tmpParcoursAller = rotir.getValue().charValue();
								System.out
										.println("La lettre " + msgEnClair.charAt(i) + " devient " + tmpParcoursAller);
								break;
							}
						}

						else {
							if (tmpParcoursRetour == rotir.getValue()) {

								tmpParcoursRetour = rotir.getKey();
								System.out.println("PASSAGE EN ARRIERE : La lettre " + msgEnClair.charAt(i)
										+ " devient " + tmpParcoursRetour);
								break;
							}
						}

					}

					System.out.println("RAPPEL : " + rot.getValue().getAssociationCharacter());
//					System.out.println("TEST   " + rot.getKey() + " " + rot.getValue().getBranchement());
					System.out.println("INITIAL  :" + rot.getValue().getPositionInitiale() + " => COURANT "
							+ rot.getValue().getPositionCourant());

				}
				//PASSAGE REFLECTEUR
				if (compteur == 2 && reflecteur.isEstPasse() == false) {
					for (Entry<Character, Character> ref : reflecteur.getReflecteur().entrySet()) {

						if (tmpParcoursAller == ref.getKey()) {
							System.out.println("\n=> Passe dans le Reflecteur ");
							tmpParcoursAller = ref.getValue();
							System.out.println(
									"REFLECTEUR : La lettre " + msgEnClair.charAt(i) + " devient " + tmpParcoursAller);
							tmpParcoursRetour = tmpParcoursAller;
							reflecteur.setEstPasse(true);
							compteurReflecteur += 3;
							break;
						}

					}
					compteur -= 1;
				}

				if (compteur == 2) {
					//PASSAGE PLUGBOARD RETOUR
					if (plugboard.isEstPasse() == true) {
						System.out
								.println("\n=> Passe dans la Table de Connexion RETOUR : " + plugboard.getPlugboard());
						for (Entry<Character, Character> table : plugboard.getPlugboard().entrySet()) {
							if (tmpParcoursRetour == table.getKey()) {
								tmpParcoursRetour = table.getValue();
								System.out
										.println("La lettre " + msgEnClair.charAt(i) + " devient " + tmpParcoursRetour);

								break;
							}
							plugboard.setEstPasse(false);
						}
					}
				}

				System.out.println("LE COMPTEUR => " + compteur);

			} while (compteur == 1);

			reflecteur.setEstPasse(false);

			if (i == 0 && code.isEmpty() == false) {
				code.clear();
			}

			code.add(tmpParcoursRetour);

			compteur = 1;
			//INVERSION DU HASHMAP POUR LE PASSAGE A L'ARRIERE
			rotors.invertMap();

		}

		return code;
	}

	/**
	 * @function Main - Lancement de la machine Enigma avec pour configuration PLUGBOARD, ROTORS (POSITION TRIGGER ET DEPART) et INPUT MESSAGE
	 * @param args
	 */
	public static void main(String[] args) {

	
		int m = 1;
		int t = 1;
		int cpt = 9;
		String resultbinary;
		String choices;
		int triggerPosition = 0;

		System.out.println("Faites fonctionner la machine ENIGMA.\n\n******RAPPEL DES REGLES******\n\n*Vous devez configurer vos 10 couples de branchements (20 lettres a modifier) sur n'importe quelle lettre.\n*Vous devez choisir trois rotors a configurer (position de depart partant de 0 a 25 et position d'encoche de 0 a 25).\n*Entrez par la suite l'enchainement de votre message en clair.\n*Chaque caractere du message passera par le circuit d'ENIGMA (PLUGBOARD, ROTORS, REFLECTEURS, ROTORS, PLUGBOARD).\n*Vous aurez a la fin une liste contenant chaque lettre passee par ce circuit.\n");
		System.out.println(
				"\nVous devez proceder a differentes configurations.\n");
		System.out.println("----------------------------------------------------------");
		System.out.println("                CONFIGURATION DU PLUGBOARD                ");
		System.out.println("----------------------------------------------------------");
		System.out.println("Vous devez effectuer vos 10 branchements sur le PlugBoard :");
		for (int i = 0; i < 10; i++) {
			System.out.println("\nVous souhaitez que la lettre : ");
			Scanner brancheX = new Scanner(System.in);
			String lettreX = brancheX.nextLine();
			System.out.println("\n=> soit echanger avec la lettre : ");
			Scanner brancheY = new Scanner(System.in);
			String lettreY = brancheY.nextLine();
			plugboard.plugBoardBranche(lettreX.charAt(0), lettreY.charAt(0));
			System.out.println("Il vous reste encore a faire " + cpt-- + " couplages.");
		}
	

		System.out.println("----------------------------------------------------------");
		System.out.println("                   BRANCHEMENTS PLUGBOARD                 ");
		System.out.println("----------------------------------------------------------");
		System.out.println("Vos differents branchement du PlugBoard sont les suivants :\n");
		//plugboard.getPlugChoisis();
		
		for (Entry<Character, Character> branchementChoisis : plugboard.getPlugboard().entrySet()) {			
				System.out.println("La lettre " + branchementChoisis.getKey() + " est branchee avec la lettre "
						+ branchementChoisis.getValue());
			
		}
	
			System.out.println("----------------------------------------------------------");
			System.out.println("                   TABLE DES ROTORS  1/2                  ");
			System.out.println("----------------------------------------------------------");
			System.out.println("Voici tous les Rotors :\n");
			for(Entry<Integer, Rotor> rotis : rotors.getAllRotors().entrySet()) {
				System.out.println("ROTOR NUMERO "+ rotis.getKey() + " : \n"+ rotis.getValue().getBranchement());
			}
			
		

		
		for (int i = 1; i < 4; i++) {

			System.out.println(
					"\nParmi les rotors, vous pouvez en configurer trois.\nQuel Rotor voulez-vous choisir pour configurer ?\n");
			Scanner numeroRotor = new Scanner(System.in);
			int resultR = numeroRotor.nextInt();
			// int resultR = Integer.parseInt(numRotor);

			System.out.println("A quelle position voulez-vous le placer ?");
			Scanner positionRotor = new Scanner(System.in);
			int resultP = positionRotor.nextInt();

			// if(i==2 || i==3) {
			System.out.println("Pour quelle position voulez-vous declencher un trigger pour le prochain rotor ?");
			Scanner positionRotorTrigger = new Scanner(System.in);
			triggerPosition = positionRotorTrigger.nextInt();

			// }

			rotors.rotorChoisi(resultR, resultP, triggerPosition);
			
			
		}
		//System.out.println("BREAK");
		System.out.println("----------------------------------------------------------");
		System.out.println("            POSITIONNEMENT DES 5 AUTRES ROTORS            ");
		System.out.println("----------------------------------------------------------");
		rotors.setAutresRotors();
		

		System.out.println("----------------------------------------------------------");
		System.out.println("                CONFIGURATION DES 3 ROTORS                ");
		System.out.println("----------------------------------------------------------");
		for (Entry<Integer, Rotor> rotip : rotors.getAllRotorsChoisis().entrySet()) {
			System.out.println(
					"VOTRE ROTOR NUMERO " + rotip.getValue().getNumeroRotor() + " : " + rotip.getValue().getBranchement()
							+ " est dans la POSITION " + rotip.getValue().getPositionInitiale() + " dans la machine. ("
							+ rotip.getValue().getPositionTrigger() + ")");

		}
		System.out.println("----------------------------------------------------------");
		System.out.println("                   TABLE DES ROTORS  2/2                  ");
		System.out.println("----------------------------------------------------------");
		m = 1;
		for (Entry<Integer, Rotor> rotipii : rotors.getRotorAll().entrySet()) {
			System.out.println("\nROTOR NUMERO " + rotipii.getValue().getNumeroRotor() + " : "
					+ rotipii.getValue().getBranchement() + " est dans la position "
					+ rotipii.getValue().getPositionInitiale() + " dans la machine. \n(Position Trigger : "
					+ rotipii.getValue().getPositionTrigger() + ")" + " => (Verification position) : "
					+ rotipii.getValue().getPositionCumulee());

		}
		m = 1;

		System.out.println(
				"\nFelicitations ! Vous avez configure vos Rotors ! \nVous pouvez des a present proceder au chiffrement !");
		do {
		System.out.println("----------------------------------------------------------");
		System.out.println("                   PASSAGE AU CHIFFREMENT                 ");
		System.out.println("----------------------------------------------------------");
			Scanner code = new Scanner(System.in);
			System.out.println("\nEntrez ce que vous souhaitez chiffrez :\n ");

			String codeString = code.next();
			System.out.println("----------------------------------------------------------");
			System.out.println("                   VOTRE MESSAGE CHIFFRE                  ");
			System.out.println("----------------------------------------------------------");
			System.out.println("\nVotre message chiffre est le suivant : \n" + chiffrerMessage(codeString));
			
			//POSSIBILITE DE DECHIFFREMENT ICI - ON DOIT RESET LES POSITIONS PAR RAPPORT A CEUX DU DEBUT
			System.out.println("\nVoulez-vous reutiliser la machine Enigma ? (y/n)");
			Scanner relaunchEnigma = new Scanner(System.in);
			resultbinary = relaunchEnigma.next();

			System.out.println("Voici la configuration actuelle : ");
			for (Entry<Integer, Rotor> rotip : rotors.getRotorAll().entrySet()) {

				System.out.println("Les positions actuelles du rotor " + rotip.getValue().getNumeroRotor() + " est a la position "
						+ rotip.getValue().getPositionCourant() + " => Trigger : " + rotip.getValue().getPositionTrigger() + " Verification position : " + rotip.getValue().getPositionCumulee());

			}

			System.out.println(
					"\nVoulez-vous garder les memes configurations des Rotors pour continuer votre chiffrement ? (y/n)");
			Scanner haveChanged = new Scanner(System.in);
			choices = relaunchEnigma.next();

			if (choices.equals("n")) {
				rotors.setHasChanged(true);
		
				
				for (Entry<Integer, Rotor> rogiti : rotors.getAllRotorsChoisis().entrySet()) {
					
					System.out.println("A quelle position voulez-vous placer le ROTOR NUMERO " + rogiti.getValue().getNumeroRotor() + "?");
					Scanner positionRotor = new Scanner(System.in);
					int resultP = positionRotor.nextInt();

					System.out.println("Pour quelle position voulez-vous declencher un trigger pour le prochain rotor ?");
					Scanner positionRotorTrigger = new Scanner(System.in);
					triggerPosition = positionRotorTrigger.nextInt();

					rotors.rotorChoisi(rogiti.getValue().getNumeroRotor(), resultP, triggerPosition);
				}
				rotors.setAutresRotors();
					
//				for (Entry<Integer, Rotor> rotipii : rotors.getRotorAll().entrySet()) {
//					System.out.println("TEST Votre rotor " + rotipii.getValue().getNumeroRotor() + " : "
//							+ rotipii.getValue().getBranchement() + " est dans la position "
//							+ rotipii.getValue().getPositionInitiale() + " dans la machine. ("
//							+ rotipii.getValue().getPositionTrigger() + ")" + " => CUMUL : "
//							+ rotipii.getValue().getPositionCumulee());
//
//				}
				
				cpt =9;
				for (int i = 0; i < 10; i++) {
					System.out.println("\nVous souhaitez que la lettre : ");
					Scanner brancheX = new Scanner(System.in);
					String lettreX = brancheX.nextLine();
					System.out.println("\n=> soit echanger avec la lettre : ");
					Scanner brancheY = new Scanner(System.in);
					String lettreY = brancheY.nextLine();
					plugboard.plugBoardBranche(lettreX.charAt(0), lettreY.charAt(0));
					System.out.println("Il vous reste encore a faire " + cpt-- + " couplages.");
				}

				System.out.println("Vos différents branchement du PlugBoard sont les suivants :\n");
				//plugboard.getPlugChoisis();

				for (Entry<Character, Character> branchementChoisis : plugboard.getPlugboard().entrySet()) {					
						System.out.println("La lettre " + branchementChoisis.getKey() + " est branchee avec la lettre "
								+ branchementChoisis.getValue());
					
				}

			} else {
				continue;
			}

		} while (resultbinary.equals("y"));

	}

	


}
